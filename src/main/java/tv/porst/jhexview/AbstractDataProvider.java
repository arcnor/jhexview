package tv.porst.jhexview;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractDataProvider implements IDataProvider {
	private Set<IDataChangedListener> _listeners = new HashSet<IDataChangedListener>();

	@Override
	public void addListener(final IDataChangedListener listener) {
		_listeners.add(listener);
	}

	@Override
	public void removeListener(final IDataChangedListener listener) {
		_listeners.remove(listener);
	}

	protected void notifyListeners() {
		for (final IDataChangedListener l : _listeners) {
			l.dataChanged();
		}
	}
}
